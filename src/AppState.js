import { observable } from 'mobx';

class AppState {
  @observable count = 0

  constructor() {
  }

  increment() {
    this.count++;
  }
  
  decrement() {
    this.count--;
  }
}

export default AppState;
