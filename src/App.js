import React, { Component } from 'react';
import { observer } from 'mobx-react';
import Styles from './styles';

//Public API constants
const WIDGET_ID = 'widget1';  //Default if developer does not provide 
const API_INCREMENT = 'AS_INCREMENT_';

@observer
class App extends Component {

  constructor(props) {
    super(props);
    this.styles = Styles(props);
  }

  componentWillMount() {
    this.props.appState.patientData = JSON.parse(this.props.patientData); //props is immutable so attach to appState
    this.props.appState.widgetId = this.props.widgetId || WIDGET_ID;
  }

  componentDidMount() {
    //Mount the Public API here onto the window object for global access
    window[API_INCREMENT + this.props.appState.widgetId] = () => {
      this.handleInc();
    }
  }

  componentWillUnmount() {
    //Garbage collect
    window[API_INCREMENT + this.props.appState.widgetId] = null;
  }

  render() {
    return (
      <div style={this.styles.container}>
        WidgetID: {this.props.widgetId}<br />
        Patient: {this.props.appState.patientData.id}<br />
        Room: {this.props.appState.patientData.room}<br />
        Counter: {this.props.appState.count}<br />
        <button onClick={this.handleDec} style={this.styles.button}>-</button>
        <button onClick={this.handleInc} style={this.styles.button}>+</button>
      </div>
    );
  }

  handleDec = () => {
    this.props.appState.decrement();
  }
  handleInc = () => {
    this.props.appState.increment();
  }

}

//Default Props 
//App.propTypes = {
//widgetId: PropTypes.string.isRequired
//};
//App.defaultProps = {
//widgetId: 'widget1'  //by default the id is the same as the widget name
//};

export default App;
