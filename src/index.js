import React from 'react';
import { render } from 'react-dom';
import AppState from './AppState';
import App from './App';

const WIDGETS = document.querySelectorAll(".widget1");
let appState;

WIDGETS.forEach((widget, index) => {
  appState = new AppState(); //each widget has its own state
  render(
    <App appState={appState} {...widget.dataset} />,  //dataset supported by IE11+, may need to use element.attributes
    WIDGETS[index]
  );
});

