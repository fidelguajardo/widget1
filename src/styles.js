export default (props) => {
    return {
        container: {
            borderStyle: "solid",
            borderWidth: "9px",
            borderRadius: "10px",
            borderColor: props.borderColor || "black",
            margin: "10px",
            padding: "10px",
            color: props.textColor || "#ffffff",
            backgroundColor: props.bgColor || "#009DDC",
            width: "150px"
        },
        button: {
            fontStyle: "italic",
            color: props.btnTextColor || "black",
            backgroundColor: props.btnBgColor || "white"
        }
    }
}